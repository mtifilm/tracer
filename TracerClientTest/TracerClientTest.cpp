// TracerClientTest.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <string>
#include <windows.h> 
#include <stdio.h>
#include <conio.h>
#include <sstream>
#include <algorithm>
#include "NewTimer.h"
#include <iomanip>

#include <tchar.h>
using namespace std;

void writeString(HANDLE pipe, const string &data)
{
	const int MAX_SIZE = 1024;
	char buffer[MAX_SIZE+2];    // avoid allocation

	DWORD cbWritten = 0;
	auto len = min<int>((int)data.size(), 256);
	buffer[0] = len / 256;
	buffer[1] = len % 256;
	memcpy_s(buffer + 2, 1024, data.c_str(), len);

 	auto fSuccess = WriteFile(
		pipe,                  // pipe handle 
		buffer,             // message 
		len+2,              // message length 
		&cbWritten,             // bytes written 
		nullptr);                  // not overlapped 
}

std::string GetLastErrorAsString()
{
	//Get the error message, if any.
	DWORD errorMessageID = ::GetLastError();
	if (errorMessageID == 0)
		return std::string(); //No error message has been recorded

	LPSTR messageBuffer = nullptr;
	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

	std::string message(messageBuffer, size);

	//Free the buffer.
	LocalFree(messageBuffer);

	return message;
}

int main()
{
	const char * lpszPipename = "\\\\.\\pipe\\TracerPipe831";
	auto hPipe = CreateFile(
		lpszPipename,   // pipe name 
		GENERIC_WRITE,
		0,              // no sharing 
		NULL,           // default security attributes
		OPEN_EXISTING,  // opens existing pipe 
		0,              // default attributes 
		NULL);          // no template file 

	if (hPipe == INVALID_HANDLE_VALUE)
	{
		cout << "Error connecting to pipe, error # " << GetLastError() << endl;
		cout << GetLastErrorAsString() << endl;
		return -1;
	}
	
	NewTimer hrt;
	for (auto i = 0; i < 50; i++)
	{
		ostringstream os;
		os << showpoint << setw(6) <<  hrt.elapsedMilliseconds() <<  ":  Test of c++ code at " << i << "\n";
		writeString(hPipe, os.str());
	}

	
    return 0;
}

