﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Pipes;
using System.Security.Principal;
using System.Diagnostics;
using System.Threading;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Concurrent;

namespace TracerServer
{
    class TracerServerModel : BindableObject, IDisposable
    {
        private StreamString streamStream;

        private bool disposedValue = false; // To detect redundant calls
        private string statusString;
        private string pipeName;
        private int maxThreads;
        private List<Thread> servers = new List<Thread>();
        private List<NamedPipeServerStream> serverStreams = new List<NamedPipeServerStream>();
        private object locker = new object();
        private string tracerString;

        public TracerServerModel()
        {
        }

        public ConcurrentQueue<string> TracerQueue = new ConcurrentQueue<string>();

        public void StartServer(string pipeName, int maxThreads = 4)
        { 
            this.pipeName = pipeName;
            this.maxThreads = maxThreads;

            // This is not just for show.  The bindable object is not thread safe
            // so start it up
            this.StatusString = "Tracer started " + DateTime.Now.ToShortDateString();

            for (var i = 0; i < maxThreads; i++)
            {
                servers.Add(new Thread(readPipeThread));
                servers[i].IsBackground = true;
                servers[i].Start();
            }
        }

        private void readPipeThread()
        {
            while (true)
            {
                NamedPipeServerStream pipeServer = 
                new NamedPipeServerStream(this.pipeName, PipeDirection.In, this.maxThreads);

                this.serverStreams.Add(pipeServer);

                int threadId = Thread.CurrentThread.ManagedThreadId;

                this.StatusString = $"Waiting for client on [{threadId}]";

                // Wait for a client to connect
                pipeServer.WaitForConnection();

                this.StatusString = $"Client connected on thread[{threadId}] at " + DateTime.Now.ToLongDateString() + "," + DateTime.Now.ToLongTimeString();
                this.streamStream = new StreamString(pipeServer);

                try
                {
                    // We just sit here forever
                    while (true)
                    {
                        var x = this.streamStream.ReadString();
                        if (pipeServer.IsConnected == false)
                        {
                            break;
                        }

                        // We need to loop here and load the queue
                        this.TracerString = x;
                    }
                }
                catch (IOException e)
                {
                    // Catch the IOException that is raised if the pipe is broken
                    // or disconnected.
                    this.StatusString = $"I/O error: {e.Message}";
                }
                catch (Exception e)
                {
                    // This is on close and threads aborted
                    this.StatusString = $"ERROR: {e.Message}";
                }

                pipeServer.Close();
            }
        }

        #region IDisposable Support

        public string StatusString
        {
            get
            {
                return this.statusString;
            }

            set
            {
                lock (this.locker)
                {
                    this.statusString = value;
                    this.RaisePropertyChanged("StatusString");
                }
            }
        }

        public string TracerString
        {
            get
            {
                return this.tracerString;
            }

            set
            {
                lock (this.locker)
                {
                    this.tracerString = value;
                    this.RaisePropertyChanged("TracerString");
                }
            }
        }

  

        public void CloseClients()
        {
            // Not sure if this should be here
            foreach (var readThread in this.serverStreams)
            {
                // readThread.Disconnect();
                readThread.Close();
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // Give the client process some time to display results before exiting.
                    Thread.Sleep(100);
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }


        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
