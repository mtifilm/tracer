﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.Pipes;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.IO;
using System.Windows;
using System.Windows.Data;

namespace TracerServer
{
    class TracerServerViewModel
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////
        #region Private fields

        /// <summary>
        /// Locking object
        /// </summary>
        private object locker = new object();

        /// <summary>
        /// The reference to the Status tab's textbox
        /// </summary>
        private RichTextBox statusTextBox;

        /// <summary>
        /// The reference to the Tracer tab's textbox
        /// </summary>
        private RichTextBox tracerTextBox;

        /// <summary>
        /// The currently active list of TextPointers defining the search matches for the current text box's document
        /// The starting TextPointer of a match is 2 * the match index.
        /// The ending TextPointer of a match is 2 * the match index + 1.
        /// </summary>
        private List<TextPointer> matches;

        /// <summary>
        /// Stores the lists of TextPointers of search matches for both the filtered and unfiltered versions for each tab.
        /// The index for unfiltered matches is 2 * a tab's index.
        /// The index for filtered matches is 2 * a tab's index + 1.
        /// </summary>
        private Dictionary<int, List<TextPointer>> searchMatches  = new Dictionary<int, List<TextPointer>>();

        /// <summary>
        /// Stores the FlowDocument content for both the filtered and unfiltered versions for each tab.
        /// The index for the unfiltered text is 2 * a tab's index.
        /// The index for filtered text is 2 * a tab's index + 1.
        /// </summary>
        private Dictionary<int, FlowDocument> storedDocuments = new Dictionary<int, FlowDocument>();

        /// <summary>
        /// The color to highlight the current selected search match
        /// </summary>
        private SolidColorBrush selectedMatchColor = new SolidColorBrush(Colors.Orange);

        /// <summary>
        /// The color to highlight the current unselected search matches
        /// </summary>
        private SolidColorBrush unselectedMatchColor = new SolidColorBrush(Colors.Bisque);
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors
        public TracerServerViewModel()
        {

        }
        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the text in the search bar
        /// </summary>
        public string SearchBarText{ get; set;}

        /// <summary>
        /// Gets or sets whether the text filters only lines with a search match
        /// </summary>
        public bool FilterMode { get; set; }

        /// <summary>
        /// Gets or sets whether search/filter should utilize regular expressions.
        /// </summary>
        public bool UseRegex { get; set; }
        
        /// <summary>
        /// Gets or sets the TracerModel object.
        /// </summary>
        public TracerServerModel TracerModel { get; set; }

        /// <summary>
        /// Gets or sets the textbox behavior to autoscroll
        /// </summary>
        public bool AutoScroll { get; internal set; }
       
        /// <summary>
        /// Gets or sets the index of the currently selected search match.
        /// </summary>
        public int SelectedMatchIndex { get; set; }
        
        /// <summary>
        /// Gets or sets the currently active tab by index.
        /// </summary>
        public int CurrentTab { get; set; }
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        public void StartServer()
        {
            this.TracerModel = new TracerServerModel();
            this.TracerModel.PropertyChanged += TracerModelPropertyChanged;
            this.TracerModel.StartServer("TracerPipe831");
        }

        public void StopServer()
        {
            this.TracerModel.PropertyChanged -= TracerModelPropertyChanged;
        }

        internal void ClearTracerText()
        {
            this.tracerTextBox.Document = storedDocuments[0];
            storedDocuments[1] = null;
            this.matches = null;
            this.searchMatches[0] = null;
            this.searchMatches[1] = null;
            this.tracerTextBox.SelectAll();
            this.tracerTextBox.Selection.Text = "";
            
        }

        internal void ClearStatusText()
        {
            this.statusTextBox.Document = storedDocuments[2];
            storedDocuments[3] = null;
            this.matches = null;
            this.searchMatches[2] = null;
            this.searchMatches[3] = null;
            this.statusTextBox.SelectAll();
            this.statusTextBox.Selection.Text = "";
        }

        internal async Task TestClientAsync()
        {
            await Task.Run(() => TestClient());
        }

        internal int lineCount = 0;

        internal void TestClient()
        {
            var pipeClient = new NamedPipeClientStream(".",
                "TracerPipe831",
                PipeDirection.Out,
                PipeOptions.None,
                TokenImpersonationLevel.Impersonation);

            var streamStream = new StreamString(pipeClient);

            pipeClient.Connect();
            HighResolutionTimer timer = new HighResolutionTimer();
            timer.Start();

            foreach (var i in Enumerable.Range(0, 600))
            {
                var time = timer.Milliseconds().ToString("#0000.000");
                streamStream.WriteString($"{time}: We are sending string {this.lineCount++}");
            }


            pipeClient.Close();
        }

        internal async Task SendLinesAsync()
        {

            var lines = System.IO.File.ReadAllLines(@"..\..\..\TestLog.log");
            await Task.Run(() => SendLines(lines));
        }

        internal void SendLines(string[] lines)
        {
            var pipeClient = new NamedPipeClientStream(".",
                "TracerPipe831",
                PipeDirection.Out,
                PipeOptions.None,
                TokenImpersonationLevel.Impersonation);

            var streamStream = new StreamString(pipeClient);

            pipeClient.Connect();
            foreach (var line in lines)
            {
                streamStream.WriteString(line);
            }


            pipeClient.Close();
        }

        internal void SetTracerTextBox(RichTextBox tracerTextBox)
        {
            this.tracerTextBox = tracerTextBox;
            storedDocuments.Add(0, tracerTextBox.Document);
            storedDocuments.Add(1, null);
        }

        internal void SetStatusTextBox(RichTextBox statusTextBox)
        {
            this.statusTextBox = statusTextBox;
            storedDocuments.Add(2, this.statusTextBox.Document);
            storedDocuments.Add(3, null);
        }

        public void Search(string searchText)
        {
            RichTextBox textBox = GetCurrentTextBox();
            TextRange  textRange = new TextRange(textBox.Document.ContentStart, textBox.Document.ContentEnd);
            textRange.ClearAllProperties();

            if (searchText == null || textBox == null || searchText.Equals(""))
            {
                this.matches = null;
                storedDocuments[2*this.CurrentTab + 1] = null;
                try
                {
                    TextRange unfilteredDocumentText = new TextRange(storedDocuments[2 * this.CurrentTab].ContentStart, storedDocuments[2 * this.CurrentTab].ContentEnd);
                    unfilteredDocumentText.ClearAllProperties();
                    textBox.Document = storedDocuments[2 * this.CurrentTab];
                }
                catch { }
                storedDocuments[2 * this.CurrentTab] = null;
                this.searchMatches[2 * this.CurrentTab] = null;
                this.searchMatches[2 * this.CurrentTab + 1] = null;
                return;
            }

            string pattern;
            if (this.UseRegex)
            {
                pattern = searchText;
            }
            else
            {
                pattern = Regex.Escape(searchText);
            }

            storedDocuments[2 * this.CurrentTab + 1] = new FlowDocument();
            storedDocuments[2 * this.CurrentTab + 1].PageWidth = GetCurrentTextBox().Document.PageWidth;
            TextRange filteredRange = new TextRange(storedDocuments[2 * this.CurrentTab + 1].ContentStart, storedDocuments[2 * this.CurrentTab + 1].ContentEnd);
            using (MemoryStream ms = new MemoryStream())
            {
                textRange.Save(ms, DataFormats.Xaml, true);
                string rtfText = ASCIIEncoding.Default.GetString(ms.ToArray());
                ms.Position = 0;
                filteredRange.Load(ms, DataFormats.Xaml);
                textRange.Load(ms, DataFormats.Xaml);
            }

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            MatchCollection searchMatches = rgx.Matches(textRange.Text.Replace("\r\n", "\\r\\n"));

            if (searchMatches.Count > 0)
            {
                SelectedMatchIndex = 0;

                
                this.searchMatches[2 * this.CurrentTab] = new List<TextPointer>();
                this.searchMatches[2 * this.CurrentTab + 1] = new List<TextPointer>();
                
                HighlightSearchTextMatches(searchMatches, textRange, filteredRange);

                if (storedDocuments[2 * this.CurrentTab] == null)
                {
                    storedDocuments[2 * this.CurrentTab] = GetCurrentTextBox().Document;
                }
                Filter(storedDocuments[2 * this.CurrentTab + 1], this.searchMatches[2 * this.CurrentTab + 1]);
                
                if (FilterMode)
                {
                    GetCurrentTextBox().Document = storedDocuments[2 * this.CurrentTab + 1];
                    this.matches = this.searchMatches[2 * this.CurrentTab + 1];
                }
                else
                {
                    this.matches = this.searchMatches[2 * this.CurrentTab];
                }
                ScrollToCurrentSearchMatch();
            }
            else
            {
                this.matches = null;
                storedDocuments[2 * this.CurrentTab + 1] = null;
            }
        }

        public void Filter(FlowDocument toFilter, List<TextPointer> matchPointers)
        {
            int matchIndex = 0;
            List<Block> blocks = toFilter.Blocks.ToList();
            for (int i = 0; i < blocks.Count; i++)
            {
                Block block = blocks[i];
                TextRange blockTextRange = new TextRange(block.ContentStart, block.ContentEnd);

                while (matchIndex < matchPointers.Count && matchPointers[matchIndex].CompareTo(block.ContentStart)<0)
                {
                    matchIndex += 2;                    
                }

                if (matchIndex == matchPointers.Count || !blockTextRange.Contains(matchPointers[matchIndex]))
                {
                    toFilter.Blocks.Remove(block);
                }
            }
        }

        public void HighlightSearchTextMatches(MatchCollection searchMatches, TextRange textRange, TextRange filteredRange)
        {
            if (searchMatches == null || searchMatches.Count <= 0)
            {
                return;
            }

            //Iterate in reverse to avoid offsetting the pointers
            for (int i = searchMatches.Count - 1; i >= 0; i--)
            {
                Brush highlightColor = (i == this.SelectedMatchIndex) ? selectedMatchColor : unselectedMatchColor;

                int startIndex = searchMatches[i].Index;
                int endIndex = startIndex + searchMatches[i].Length;

                TextPointer start = textRange.Start.GetPositionAtOffset(startIndex, LogicalDirection.Forward);
                TextPointer end = textRange.Start.GetPositionAtOffset(endIndex, LogicalDirection.Backward);

                try
                {
                    ApplySearchTextHighlight(start, end, highlightColor);
                    this.searchMatches[2 * this.CurrentTab].Add(end);
                    this.searchMatches[2 * this.CurrentTab].Add(start);
                }
                catch { }
                TextPointer start2 = filteredRange.Start.GetPositionAtOffset(startIndex, LogicalDirection.Forward);
                TextPointer end2 = filteredRange.Start.GetPositionAtOffset(endIndex, LogicalDirection.Backward);
                try { 
                ApplySearchTextHighlight(start2, end2, highlightColor);
                this.searchMatches[2 * this.CurrentTab + 1].Add(end2);
                this.searchMatches[2 * this.CurrentTab + 1].Add(start2);
                }
                catch { }
            }
            this.searchMatches[2 * this.CurrentTab].Reverse();
            this.searchMatches[2 * this.CurrentTab + 1].Reverse();
        }

        public void ApplySearchTextHighlight(TextPointer start, TextPointer end, Brush highlightColor)
        {
            TextRange selected = new TextRange(start, end);
            selected.ClearAllProperties();
            selected.ApplyPropertyValue(TextElement.BackgroundProperty, highlightColor);
        }

        public void ScrollToCurrentSearchMatch()
        {
            RichTextBox currentTextBox = GetCurrentTextBox();
            currentTextBox.Focus();
            TextPointer currentMatch = (2 * this.SelectedMatchIndex < matches?.Count) ? matches[2 * this.SelectedMatchIndex] : null;
            if (currentMatch != null && currentMatch.IsInSameDocument(currentTextBox.Document.ContentStart))
            {
                currentTextBox.CaretPosition = currentMatch;
            }
            else
            {
                currentTextBox.CaretPosition = currentTextBox.Document.ContentStart.GetInsertionPosition(LogicalDirection.Forward);
            }
            currentTextBox.ScrollToVerticalOffset(currentTextBox.CaretPosition.GetCharacterRect(LogicalDirection.Forward).Y);
        }

        public void NextSearchMatch()
        {
            if (this.matches != null && this.matches.Count > 2)
            {
                //Update selected match index
                int lastSelectedIndex = this.SelectedMatchIndex;
                this.SelectedMatchIndex = (this.SelectedMatchIndex + 1) % (this.matches.Count / 2);

                UpdateSelectedHighlight(lastSelectedIndex);
            }
        }

        public void PreviousSearchMatch()
        {
            if (this.matches != null && this.matches.Count > 2)
            {
                //Update selected match index
                int lastSelectedIndex = this.SelectedMatchIndex;
                this.SelectedMatchIndex -= 1;
                if (this.SelectedMatchIndex < 0)
                {
                    this.SelectedMatchIndex = (this.matches.Count / 2) - 1;
                }

                UpdateSelectedHighlight(lastSelectedIndex);
            }
        }
        public void UpdateSelectedHighlight(int lastSelectedIndex)
        {
            // Get indices for the TextPointers of the search matches
            int currentHighlightStart = 2 * this.SelectedMatchIndex;
            int curentHighlightEnd = 2 * this.SelectedMatchIndex + 1;
            int previousHighlightStart = lastSelectedIndex * 2;
            int previousHighlightEnd = lastSelectedIndex * 2 + 1;

            //Get the filtered and unfiltered lists of TextPointers for the current text box
            var unfilteredMatches = this.searchMatches[2 * this.CurrentTab];
            var filteredMatches = this.searchMatches[2 * this.CurrentTab + 1];

            //Update the highlights for current and previously selected match
            ApplySearchTextHighlight(unfilteredMatches[currentHighlightStart], unfilteredMatches[curentHighlightEnd], selectedMatchColor);
            ApplySearchTextHighlight(unfilteredMatches[previousHighlightStart], unfilteredMatches[previousHighlightEnd], unselectedMatchColor);
            ApplySearchTextHighlight(filteredMatches[currentHighlightStart], filteredMatches[curentHighlightEnd], selectedMatchColor);
            ApplySearchTextHighlight(filteredMatches[previousHighlightStart], filteredMatches[previousHighlightEnd], unselectedMatchColor);

            //Update cursor location
            ScrollToCurrentSearchMatch();
        }

        public RichTextBox GetCurrentTextBox()
        {
            switch (this.CurrentTab)
            {
                case 0://Trace
                    return tracerTextBox;

                case 1://Status
                    return statusTextBox;
            }
            return null;
        }

        public void SetActiveDocument()
        {
            if (!this.searchMatches.ContainsKey(2 * this.CurrentTab))
            {
                this.searchMatches[2 * this.CurrentTab] = new List<TextPointer>();
            }

            if (!this.searchMatches.ContainsKey(2 * this.CurrentTab+1))
            {
                this.searchMatches[2 * this.CurrentTab+1] = new List<TextPointer>();
            }

            var unfilteredMatches = this.searchMatches[2 * this.CurrentTab];
            var filteredMatches = this.searchMatches[2 * this.CurrentTab + 1];

            RichTextBox textBox = GetCurrentTextBox();

            try
            {
                ApplySearchTextHighlight(this.matches[this.SelectedMatchIndex * 2], this.matches[this.SelectedMatchIndex * 2 + 1], unselectedMatchColor);
            }
            catch { }

            if (FilterMode && storedDocuments[2 * this.CurrentTab + 1] != null)
            {
                textBox.Document = storedDocuments[2 * this.CurrentTab + 1];
                this.matches = filteredMatches;
            }
            else if (storedDocuments[2 * this.CurrentTab] != null)
            {     
                textBox.Document = storedDocuments[2 * this.CurrentTab];
                this.matches = unfilteredMatches;            
            }

            if (this.matches == null || this.matches.Count<=this.SelectedMatchIndex*2 + 1)
            {
                this.SelectedMatchIndex = 0;
            }

            try
            {
                ApplySearchTextHighlight(this.matches[this.SelectedMatchIndex * 2], this.matches[this.SelectedMatchIndex * 2 + 1], selectedMatchColor);
            } catch { }

            ScrollToCurrentSearchMatch();
        }
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        private void TracerModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var tid = Thread.CurrentThread.ManagedThreadId;
            lock (this.locker)
            {
                switch (e.PropertyName)
                {
                    case "StatusString":
                        App.Current.Dispatcher.Invoke((Action) delegate
                        {
                            storedDocuments[2]?.Blocks.Add(new Paragraph(new Run(this.TracerModel.StatusString)));
                            this.statusTextBox?.ScrollToEnd();
                        });
                        break;

                    case "TracerString":
                        if (this.tracerTextBox == null)
                        {
                            break;
                        }

                        App.Current.Dispatcher.Invoke((Action) delegate
                        {
                            storedDocuments[0]?.Blocks.Add(new Paragraph(new Run(this.TracerModel.TracerString)));

                            //var lineCount = tracerTextBox.LineCount;
                            //if (lineCount > 1000)
                            //{
                            //    Stopwatch timer = new Stopwatch();
                            //    timer.Start();
                            //    var l = 0;
                            //    for (var i = 0; i< 500; i++)
                            //    {
                            //        l += tracerTextBox.GetLineLength(i);
                            //    }

                            //    var x = timer.ElapsedMilliseconds.ToString();
                            //    tracerTextBox.Text = tracerTextBox.Text.Remove(0, l);
                            //    var y = timer.ElapsedMilliseconds.ToString();
                            //    this.tracerTextBox.AppendText(x + ",  " + y + '\n');
                            //}

                            if (this.AutoScroll)
                            {
                                this.tracerTextBox?.ScrollToEnd();
                            }
                        });
                        break;
                }
            }
        }
        #endregion
    }
}
